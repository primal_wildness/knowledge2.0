import os
import re
import json
'''
功能：批量替换 blog 中图片的链接
原理：通过 json 文件来记录文章中图片的链接
步骤：
    1.手动修改对应图片新的链接
    2.更新记录文件，记录新添文章的图片链接
    3.将修改同步到文章中
    4.将记录的链接更新为新连接
'''

url_json = {
}

if __name__ == "__main__":
    path = "../500-Blogs/blogs"
    # 加载json
    with open('img_url.json','r', encoding='utf-8') as f:
        url_json = json.load(f)

    img_md_patten = r"!\[[^]]*]\((.*?)\)"
    img_html_patten = r'src\s*=\s*"(.*?)"'
    # step2
    for dirpath, dirnames, filenames in os.walk(path): #dfs
        for fname in filenames:
            if fname.endswith('.md'):
                #print(dirpath + os.sep + fname)
                fpath = dirpath + os.sep + fname
                with open(fpath, 'r', encoding='utf-8') as f:
                    content = f.read()
                # 查找文章中图片链接，两种形式 md 形式和 html形式
                md_img_url = re.findall(img_md_patten, content)
                html_img_url = re.findall(img_html_patten, content)
                img_urls = []
                img_urls.extend(md_img_url)
                img_urls.extend(html_img_url)
                for iurl in img_urls:
                    if iurl and iurl not in url_json:
                        print('New Article URL: ', iurl)
                        url_json[iurl] = iurl
    print("图片链接总计：",len(url_json))

    # step3
    for src_url in url_json.keys():
        if src_url != url_json[src_url]:
            for dirpath, dirnames, filenames in os.walk(path): #dfs
                for fname in filenames:
                    if fname.endswith('.md'):
                        fpath = dirpath + os.sep + fname
                        with open(fpath, 'r', encoding='utf-8', newline='\n') as f:
                            content = f.read()
                        if src_url in content:
                            new_url = url_json[src_url]
                            print(f'Replace: {src_url} -> {new_url}')
                            content = content.replace(src_url, new_url)
                            content = content.replace('\r\n', '\n')
                        # 替换
                        with open(fpath, 'w', encoding='utf-8', newline='\n') as f:
                            f.write(content)
    # step4
    for src_url in list(url_json.keys()):
        new_url = url_json[src_url]
        if src_url != new_url:
            print('Update: ', src_url)
            url_json.pop(src_url)
            url_json[new_url] = new_url
    json_str = json.dumps(url_json)
    with open("img_url.json", "w") as file:
        file.write(json_str)