
## 树状数组模板
    
```cpp
    vector<int> tree;
    // 求最低位1
  int lowbit(int i)
  {
      return i&-i;
  }
    //向后更新
  void update(int p)
  {
      int n = tree.size();
      for(;p < n; p+=lowbit(p))
          ++tree[p];
  }
    //统计前缀和
  long long tsum(int p)
  {
      long long res = 0;
      int n = tree.size();
      for(;p >= 1; p-=lowbit(p))
          res += tree[p];
      return res;
  }
```
  
> [https://leetcode.cn/problems/number-of-pairs-satisfying-inequality/](https://leetcode.cn/problems/number-of-pairs-satisfying-inequality/)
>
  
### 二维树状数组
  
```cpp
int n, m;
ll mat[5000][5000];
  
int lowbit(int i) {
    return i & -i;
}
  
long long tsum(int x, int y) {
    ll res = 0;
  
    for (int i = x; i >= 1; i -= lowbit(i))
        for (int j = y; j >= 1; j -= lowbit(j)) {
            res += mat[i][j];
        }
  
    return res;
}
  
void update(int x, int y, int value) {
    for (int i = x; i <= n; i += lowbit(i)) {
        for (int j = y; j <= m; j += lowbit(j)) {
            mat[i][j] += value;
        }
    }
}
  
//区域c 到 a， d 到 b， 闭区间
ll section_sum(int a, int b, int c, int d) {
    ll ans = tsum(c, d) - tsum(a - 1, d) - tsum(c, b - 1) + tsum(a - 1, b - 1);
    return ans;
}
```
  
> [https://loj.ac/p/133](https://loj.ac/p/133)