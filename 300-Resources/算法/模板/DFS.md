深度优先搜索



> 在二叉树中分配硬币，leetcode: 979


这道题有意思，属于是有了思路就能立刻做出来的
> [最深叶节点的最近公共祖先](https://leetcode.cn/problems/lowest-common-ancestor-of-deepest-leaves/)


### 拓扑排序
思路一：[[BFS]] 是用出入度来判断是否有环
思路二：DFS 是利用深度优先搜索来判断图中是否有环，使用多种标记（未遍历，已遍历，当前遍历）

[课程表](https://leetcode.cn/problems/course-schedule/)
[序列重建](https://leetcode.cn/problems/ur2n8P/)