## 单调栈

构建最大二叉树：最大数的左边是左子树，右边是右子树，如此递归构造
  
```cpp
//nums 是节点数组
int n = nums.size();
vector<int> stk;
vector<int> right(n, -1), left(n, -1);
//vector<TreeNode*> nodes(n);
for(int i=0; i<n; ++i)
{
    //nodes[i] = new TreeNode(nums[i]);
        //标记左子树：将小于当前元素的值设为左子树
    while(!stk.empty() && nums[stk.back()] < nums[i])
    {
        right[stk.back()] = i;  // right[a] = b; 意味着序号a是序号b的左子树
        stk.pop_back();
    }
        //标记右子树：由于左子树在上一步已经标记完成了，所以只需要再标记一次右子树
    if(!stk.empty())
    {
        left[i] = stk.back(); // left[a] = b; 意味着序号a是序号b的右子树
    }
    stk.emplace_back(i);
}
  
//后面根据right和left的情况来构建这颗二叉树（以下只是看着程序叙述，并不一定好理解，如果以后需要理解，请重新表述）
/*
1. left 和 right 都为-1的时候，则此点为根
2. right 为 -1， 或者 left不为-1且 left对应的值小于right对应的值，则此点是left对应的节点的右子树
3. 其余情况为 此点是 right对应的节点的左子树
*/
  
```
  
- 单调递增栈（存储下标）
    遍历规则：（一轮操作）若当前元素值小于栈顶，则弹出，直到栈顶大于当前元素
    一轮操作完后，栈顶表示左侧比当前元素要小的下标
    然后压入当前元素，继续遍历下一个。
- 单调递减栈（存储下标）
    栈顶表示比当前元素要大的下标(非严格)
- 进阶
    单调栈不仅可以存储左侧最值，还可以算出右侧最值信息。
  
    * 严格单调增栈，左边找小于的值，右边找小于等于的值
  
    * 严格单调减栈，左边找大于的值，右边找大于等于的值
  
    * 非严格单调增栈，左边找小于等于的值，右边找小于的值
  
    * 非严格单调减栈，左边找大于等于的值，右边找大于的值。栈顶就是左边第一个比它大或等于的数，当前元素是出栈的数右边第一个比它大的数。代码如下：
  
```cpp
{
	vector<int> nums(N);
	vector<int> stk;
	//lr[i].first 是左边大于等于第i个数的下标，lr[i].second是右边...的
	vector<pair<int,int>> lr(N, {-1,-1});
	for(int i=0; i<N; ++i)
	{
		//非严格递减栈，（入栈前）获取左边大于或等于当前元素的下标，
		//(出栈时)当前元素就是右边大于出栈元素的第一个值
		//栈递减还是递增，用这个比较符号来判断
		while(!stk.empty() && nums[i] > nums[stk.back()])
		{
			lr[stk.back()].second = nums[i];
			stk.pop_back();
		}
		lr[i].first = stk.empty() ? -1 : nums[stk.back()];
		stk.push_back(i);
	}
}
```


## 单调队列

```python
q = deque()
for i in range(n):
	# 处理队列左端
	while q and :
		q.popleft()
	# 处理队列右端
	while q and :
		q.pop()
	q.append()
# 时间复杂度为 O(n)， 因为每个元素只有一次出队入队。
```

例题： 

> leetcode: 918 环形子数组的最大和
