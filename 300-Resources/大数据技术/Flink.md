Flink 是一个分布式的流处理框架，能够对有界和无界的数据流进行高效的处理（Flink 将批处理看成是流处理的一种特殊情况，即数据流是有明确界限的）。

## 核心架构及组件

### 架构
Flink 采用分层的架构设计，由上而下分别是 API & Libraries 层、Runtime 核心层以及物理部署层。
![架构](https://github.com/wangzhiwubigdata/God-Of-BigData/raw/master/pictures/flink-stack.png)

其中 API & Libraries 这一层，Flink 又进行了更为具体的划分。

### 组件
Flink 核心架构的第二层是 Runtime 层， 该层采用标准的 Master - Slave 结构， 其中，Master 部分又包含了三个核心组件：Dispatcher、ResourceManager 和 JobManager，而 Slave 则主要是 TaskManager 进程。

Task 和 SubTask
TaskManagers 实际执行的是 SubTask，而不是 Task。
一个 Task 就是一个可以链接的最小的操作链，在执行分布式计算时，Flink 将可以链接的操作 (operators) 链接到一起，这就是 Task。