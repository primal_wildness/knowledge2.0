系统虚拟机和程序虚拟机

内存空间

```mermaid
flowchart 

classDef mcolor fill:#f96

subgraph jvm[JVM内存结构]
subgraph jvmd[JVM数据区]
heap[堆]
stack[虚拟机栈]:::mcolor 
lstack[本地方法栈]:::mcolor 
cnt[程序计数器]:::mcolor 
end

subgraph localm[本地内存]
ydata[元数据区]
dm[直接内存]
end
end

```

橙色部分是线程隔离，也就是随着线程一起生灭。

HotSpot 虚拟机，看作JVM的实现

## 垃圾回收机制

大部分语言都是用那几种方法，比如lua等。请看[[垃圾回收机制]]

## 内存分配机制

## 类加载

类生命周期

加载，验证，准备

另外注意这里的几个阶段是按顺序开始，而不是按顺序进行或完成，因为这些阶段通常都是互相交叉地混合进行的，通常在一个阶段执行的过程中调用或激活另一个阶段。

初始化

- new， static
- main主类
- 反射
- 继承

谈论 JVM 的无关性，主要有以下两个：

- 平台无关性：任何操作系统都能运行 Java 代码
- 语言无关性： JVM 能运行除 Java 以外的其他代码

JVM 只认识 .class 文件，它不关心是何种语言生成了 .class 文件。