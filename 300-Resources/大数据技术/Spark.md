## 简介
分布式计算框架
支持批处理和流处理
由 Scala 语言开发

## 核心组件
Spark 基于 Spark Core 建立了 Spark SQL、Spark Streaming、MLlib、GraphX、SparkR等核心组件。

位于底层的 Spark Core 实现了Spark的作业调度、内存管理、容错、与存储系统交互等基本功能，并针对弹性分布式数据集提供了丰富的操作。

```mermaid
flowchart TD
ssql[Spark SQL]
sstr[Spark Streaming]
ml[MLlib 机器学习]
gx[图计算]
asc[Apache Spark Core]
ssql --> asc
sstr --> asc
ml --> asc
gx --> asc
asc
```

Spark 能够在内存中进行高效运算，而 MR 是基于磁盘进行数据处理的，但是快的根本原因是 Spark 的 DAG 计算模型。

### Spark SQL
主要用于操作结构化数据
为了支持结构化数据的处理，Spark SQL 提供了新的数据结构 DataFrame。类似于 Python 中的 DataFrame。

RDD 则是处理非结构化数据。
DataSet 集成了 RDD 和 DataFrame 的优点，具备强类型的特点，同时支持 Lambda 函数，但只能在 Scala 和 Java 语言中使用。

### RDD
全称为 Resilient Distributed Datasets，是 Spark 最基本的数据抽象。代表一个不可变、可分区、里面元素可并行计算的集合。
Resilient 表述数据可以存储在内存中或者磁盘中

### Spark Streaming
主要用于快速构建可扩展，高吞吐量，高容错的流处理程序。
Spark Streaming 的本质是微批处理，它将数据流进行极小粒度的拆分，拆分为多个批处理，从而达到接近于流处理的效果。而 storm 和 Flink 才是真正意义上的流计算框架。

Spark Streaming 提供称为离散流 (DStream) 的高级抽象，用于表示连续的数据流。 DStream 可以从来自 Kafka，Flume 和 Kinesis 等数据源的输入数据流创建，也可以由其他 DStream 转化而来。在内部，DStream 表示为一系列 RDD。


## DAG计算模型

为什么 Spark 能够比 [[MapReduce]] 快，原因有以下几个方面。

1. DAG计算模型的迭代优势（最重要的）
2. 少磁盘IO
3. 少 Shuffle 次数
4. 细粒度差异

MR 的设计是将中间结果保存在文件中，提高可靠性，减少内存占用，也就牺牲了性能。Spark 的设计是将中间结果存储在内存，自然就快，虽然可靠性不高，但是得益于Spark的RDD和DAG（有向无环图），其中DAG记录了job的stage以及在job执行过程中父RDD和子RDD之间的依赖关系。中间结果能够以RDD的形式存放在内存中，极大减少了磁盘IO。
(注意：不是说 spark 不写磁盘)

DAG 相比MapReduce 在大多数情况下可以减少 shuffle 次数。

MR 采用多进程模型，每次启动都需要重新申请资源，消耗了不必要的时间。
Spark 采用多线程模型，通过复用线程池中的线程来减少启动、关闭task所需要的开销。
（多线程模型也有缺点，由于同节点上所有任务运行在一个进程中，因此，会出现严重的资源争用，难以细粒度控制每个任务占用资源）

### 迭代优势
mapreduce框架中，一个程序只能拥有一个map一个reduce的过程，如果运算逻辑很复杂，一个map+一个reduce是表述不出来的，可能就需要多个map-reduce的过程。多个 map-reduce 过程每次结果都会写入 HDFS 中，这样就会很慢了。
spark框架，则**可以把上面的mapreduce-mapreduce-mapreduce的过程，连续执行，不需要反复落地到HDFS，这样就会比mapreduce快很多**

mr 和 spark 如果真要比，那么也是多个MR组成的复杂Job来比。

Spark的DAG实质上就是把计算和计算之间的编排变得更为细致紧密，使得很多MR任务中需要落盘的非Shuffle操作得以在内存中直接参与后续的运算，并且由于算子粒度和算子之间的逻辑关系使得其易于由框架自动地优化。

另外在进行复杂计算任务的时候，**Spark的错误恢复机制在很多场景会比MR的错误恢复机制的代价低，这也是性能提升的一个点**

### MR流程
从流程实现角度来看，两者也有不少差别。Hadoop MapReduce 将处理流程划分出明显的几个阶段：map, spill, merge, shuffle, sort, reduce等。每个阶段各司其职，可以按照过程式的编程思想来逐一实现每个阶段的功能。在 Spark 中，没有这样功能明确的阶段，只有不同的 stage 和一系列的 transformation，所以 spill, merge, aggregate 等操作需要蕴含在 transformation中。

### 宽依赖和窄依赖

一个 RDD 由一个或者多个分区（Partitions）组成。对于 RDD 来说，每个分区会被一个计算任务所处理，用户可以在创建 RDD 时指定其分区个数，如果没有指定，则默认采用程序所分配到的 CPU 的核心数。

- 窄依赖：父 RDDs 的一个分区最多被子 RDDs 一个分区所依赖
- 宽依赖：父 RDDs 的一个分区最多被子 RDDs 多个分区所依赖

两者的计算过程，以及错误恢复都有所差别。
宽依赖计算好所有父分区数据后还需要shuffle，窄依赖则不需要。
在数据恢复时，窄分区只需要重新对丢失分区的父分区进行计算（为什么不需要shuffle？ 也许是依赖单一所以计算结果不需要入磁盘）

### stage划分

DAG根据依赖关系的不同将 DAG 划分为不同的计算阶段 (Stage)

对于窄依赖，由于分区的依赖关系是确定的，其转换操作可以在同一个线程执行，所以可以划分到同一个执行阶段。
对于宽依赖，由于 Shuffle 的存在，只能在父 RDD(s) 被 Shuffle 处理完成后，才能开始接下来的计算。所以要划分 stage。

## 算子

算子分为 Transformation 算子 和 Action 算子。前者为 lazy ，后者为 non-lazy。

### Transformation 算子
- map 算子
将原来 RDD 的每个数据项通过 map 中的用户自定义函数 f 映射转变为一个新的元素。源码中 map 算子相当于初始化一个 RDD， 新 RDD 叫做 MappedRDD
- mapPartitions
- groupByKey
- partitionBy
- filter
- union
使用 union 函数时需要保证两个 RDD 元素的数据类型相同，返回的 RDD 数据类型和被合并的 RDD 元素数据类型相同，并不进行去重操作，保存所有元素。
- filter 函数功能是对元素进行过滤，对每个元素应用 f 函 数， 返回值为 true 的元素在 RDD 中保留，返回值为 false 的元素将被过滤掉。
- 等

### Action 算子
- foreach
- reduce
- aggregate
- 等

### 常见的宽窄依赖算子

- 宽依赖的算子
join (非hash-partitioned)、groupByKey、partitionBy

- 窄依赖的算子
map、filter、union、join (hash-partitioned)、mapPartitions


## 集群

### 配置
在原来的基础上添加

 - hadoop 配置，`core-site.xml`
```xml
<property>
	<name>hadoop.proxyuser.spark.hosts</name>
	<value>*</value>
</property>
<property>
	<name>hadoop.proxyuser.spark.groups</name>
	<value>*</value>
</property>
```


- 环境变量
``` bash
export SPARK_HOME=/opt/spark-2.1.0-bin-hadoop2.7
export PATH=$JAVA_HOME/bin:$SPARK_HOME/bin:$PATH
```

- 进入 `spark-2.1.0-bin-hadoop2.7/conf` 复制 `spark-env.sh.template` 并重命名为 `spark-env.sh`，编辑 `spark-env.sh` 文件
``` bash
export JAVA_HOME=/app/jdk-21
export SPARK_MASTER_IP=192.168.241.132   #todo
export SPARK_WORKER_MEMORY=8g
export SPARK_WORKER_CORES=4
export SPARK_EXECUTOR_MEMORY=4g
export HADOOP_HOME=/app/hadoop-3.3.6/
export HADOOP_CONF_DIR=/app/hadoop-3.3.6/etc/hadoop
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/app/jdk-21/jre/lib/amd64

```

- 把 `slaves.template` 拷贝为 slaves, 并编辑 slaves文件