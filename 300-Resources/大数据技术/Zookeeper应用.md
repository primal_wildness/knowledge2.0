## 安装
---
Zookeeper是Hadoop分布式调度服务，用来构建分布式应用系统

- 修改配置

```bash
cp zoo_sample.cfg zoo.cfg
```

```
tickTime=2000
# 改成其他目录
dataDir=/Users/tom/zookeeper   
clientPort=2181
```

- 环境变量

```bash
# ZooKeeper提供了一些可执行程序的工具，为了方便起见，我们将这些工具的路径加入到PATH环境变量中
export ZOOKEEPER_HOME=~/sw/zookeeper-x.y.z
export PATH=$PATH:$ZOOKEEPER_HOME/bin
```

- 启动服务端

```bash
zkServer.sh start

# 重启
zkServer.sh restart
```

- 检查
先在 `zoo.cfg`中添加 `4lw.commands.whitelist=*`，然后检查
```bash
echo ruok | nc localhost 2181
# 回应
# imok
```

## 原理与概念
---
ZooKeeper 是一个高可用的高性能调度服务，高性能使得 ZooKeeper 能够应用于对系统吞吐有明确要求的大型分布式系统。

ZooKeeper的**高可用性**和**快速恢复性**，高可用解决了分布式单点[[大数据技术总概]]问题。

ZooKeeper包含一个树形的数据模型，我们叫做znode。

从概念上来看，ZooKeeper其实是很简单的。他所做的一切就是保证每一次对znode树的修改，都能够复制到ensemble的大多数服务器上。

如果非主服务器脱离集群，那么至少有一台服务器上的副本保存了最新状态。剩下的其他的服务器上的副本，会很快更新这个最新的状态。

### 数据模型

ZooKeeper包含一个树形的数据模型，我们叫做znode。一个znode中包含了**存储的数据**和**ACL（Access Control List，一种类似Unix文件系统的权限控制）**。数据的访问被定义成原子性的。

节点分为两类：

1. 持久节点（persistent）：节点一旦创建，除非被主动删除，否则一直存在
2. 临时节点（ephemeral）：一旦创建该节点的客户端会话失效，则所有该客户端创建的临时节点都会被删除

客户端可以选择对某个znode进行监听，当这个znode发生变化时，会主动通知监听了这个znode的客户端。
## 集群安装与使用
---
### 安装集群

1. 创建 myid

先在服务器 hadoop102 上操作，来到配置好的目录 `dataDir` 中，创建文件 myid（文件必须叫 myid，从源码中可以看到读取的文件就是 myid）

然后在文件中添加与 server 对应的编号

每个服务器都同样操作

1. 在配置文件 zoo.cfg 的最下面增加如下的配置：

```bash
server.2=hadoop102:2888:3888
server.3=hadoop103:2888:3888
server.4=hadoop104:2888:3888

# 格式： server.A=B:C:D
# A 是一个数字，表示这个是第几号服务器
# B 是这个服务器的ip地址
# C 是这个服务器 Follower 与集群中的 Leader 服务器交换信息的端口
# D 是万一集群中的 Leader 服务器挂了，需要一个端口来重新进行选举，选出一个新的 Leader，而这个端口就是用来执行选举时服务器相互通信的端口。
```

1. 端口 or 防火墙

防火墙默认是关闭的，不需要管

分别到每台服务器启动 Zookeeper 服务端

分别在每台服务器查看状态，Mode: leader 表示该节点是集群中的 leader 节点，follower 表示该节点是集群中的 follower 节点。

### 使用

进入交互界面`./zkCli.sh`

```bash
# 查看节点
ls /
```

### 增加或者减少机器（容器） #todo