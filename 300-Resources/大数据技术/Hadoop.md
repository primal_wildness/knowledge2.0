Hadoop 是大数据开发所使用的一个核心框架。
根据Google开源的三篇大数据论文设计的，一个能够允许大量数据在计算机集群中，通过使用简单的编程模型进行分布式处理的框架

另外Hadoop也不是目前大数据的唯一解决方案，还有像Amazon的大数据技术方案等等


数据都是分布式存储的，所以需要分布式存储，分布式计算，分布式调度。
相当于你平时用单机处理数据，现在都变成分布式了。

[下载hadoop](https://www.apache.org/dyn/closer.cgi/hadoop/common)
Hadoop的核心组件包括[[HDFS]]、YARN、MapReduce、Hadoop Common、Hadoop Ozone和Hadoop Archives。后三者分别是基本组件工具库，对象存储层，归档文件处理。

# 基础Hadoop

### 集群安装
---
1. 配置环境变量
```bash
export HADOOP_HOME=/app/hadoop-3.3.6
export PATH=${HADOOP_HOME}/bin:$PATH
source /etc/profile
```

2. 修改配置
- 指定JDK
```bash
# 指定 JDK
# ${HADOOP_HOME}/etc/hadoop/hadoop-env.sh
export JAVA_HOME=/app/jdk-21
```

- 指定 namenode 的 hdfs 协议文件系统的通信地址， `core-site.xml`
```xml
<configuration>
    <property>
        <!--指定 namenode 的 hdfs 协议文件系统的通信地址-->
        <name>fs.default.name</name>
        <value>hdfs://hadoop001:8020</value>
    </property>
    <property>
        <!--指定 hadoop 集群存储临时文件的目录-->
        <name>hadoop.tmp.dir</name>
        <value>/home/hadoop/tmp</value>
    </property>
</configuration>
```

- 指定 namenode 节点数据（即元数据）的存放位置，可以指定多个目录实现容错，多个目录用逗号分隔，`hdfs-site.xml`
```xml
<property>
	<!--指定 namenode 的 hdfs 协议文件系统的通信地址。有些教程没有这个配置，但是我没有这个就会报错-->
	<name>fs.default.name</name>
	<value>hdfs://hadoop001:8020</value>
</property>
      <!--namenode 节点数据（即元数据）的存放位置，可以指定多个目录实现容错，多个目录用逗号分隔-->
<property>
    <name>dfs.namenode.name.dir</name>
    <value>/home/hadoop/namenode/data</value>
</property>
<property>
      <!--datanode 节点数据（即数据块）的存放位置-->
    <name>dfs.datanode.data.dir</name>
    <value>/home/hadoop/datanode/data</value>
</property>
```

- 配置 NodeManager 上运行的附属服务，比如 mapreduce。`yarn-site.xml`
```xml
<property>
	<!--配置 NodeManager 上运行的附属服务。需要配置成 mapreduce_shuffle 后才可以在 Yarn 上运行 MapReduce 程序。-->
	<name>yarn.nodemanager.aux-services</name>
	<value>mapreduce_shuffle</value>
</property>
<property>
	<!--resourcemanager 的主机名-->
	<name>yarn.resourcemanager.hostname</name>
	<value>hadoop001</value>
</property>
```

- 指定 mapreduce 作业运行在 yarn 上。`mapred-site.xml`
```xml
<property>
	<!--指定 mapreduce 作业运行在 yarn 上-->
	<name>mapreduce.framework.name</name>
	<value>yarn</value>
</property>
```

- 在 `/etc/hadoop/workers` 配置所有从属节点的主机名或 IP 地址，每行一个。（hadoop3.0以后slaves更名为workers）
```txt
hadoop001
hadoop002
hadoop003
# 删掉 localhost 
```

> 注意：
> 1. 容器上的 hostname 要注意操作，毕竟无法直接修改。要么 run 的时候添加，要么在 dockerfile 的时候添加。

3. 分发程序
将配置好的 hadoop 分发到其他两台服务器
```bash
scp -r /app/hadoop-3.3.6 hadoop002:/app/
scp -r /app/hadoop-3.3.6 hadoop002:/app/
```

4. 初始化，启动并查看集群

```bash
# 配置环境变量
export HDFS_NAMENODE_USER=root
export HDFS_DATANODE_USER=root
export HDFS_SECONDARYNAMENODE_USER=root
export YARN_RESOURCEMANAGER_USER=root
export YARN_NODEMANAGER_USER=root
export HADOOP_HOME=/app/hadoop-3.3.6
export HADOOP_CONF_DIR=$HADOOP_HOME/etc/hadoop
```

``` bash
# 初始化文件系统，不要多次执行
# ${HADOOP_HOME}/bin/
hdfs namenode -format

# 约倒数第十行提示信息，会提示初始化成功，"... has been successfully formatted."
```

启动集群
```bash
# ${HADOOP_HOME}/sbin/
start-all.sh

# 这个命令相当于以下两个步骤
# 启动dfs服务：start-dfs.sh
# 启动yarn服务：start-yarn.sh

# 注意，ssh 还需要配置免密登录
```

### 配置免密登录
---
```bash
# 1.生成公钥私钥对，过程中一直按空格即可，会提示生成的位置
# 添加 -f 参数是为了实现非交互式，方便脚本
ssh-keygen -t rsa -P '' -f ~/.ssh/id_rsa

# 2. 将 hadoop001 的公钥写到本机和远程机器的  ~/.ssh/authorized_keys 文件中
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
# copy-id 有权限问题，先暂时不用
# 把公钥复制到另外两条机器上

# 验证， 不用密码认证就可以登陆了
ssh hadoop002
ssh hadoop003

```

若免密登录不生效，那么查看并修正配置 `/etc/ssh/sshd_config` 
```text
# 允许root账户远程登录
PermitRootLogin yes

# 关闭严模式（重点关注这个：之前因为这个是yes 导致远程免密不生效）
StrictModes no
```

校验配置
```bash
sshd -t
```

最后，这个配置是单向的。原理是机器A把公钥存储在机器B上，当A登录B的时候，B向A发送一段随机字符串，A使用私钥加密后发送给B，B用公钥解密，若成功就证明A是可信的，允许A登录。
如果是双向免密时，那么不可以使用相同的密钥。

部署后才知道 ip 地址，那么部署前怎么配置hosts？


### 查看
---
1. 使用 jps 查看，结果如下
```bash
3892 Jps
1430 DataNode
3116 NameNode
1613 SecondaryNameNode
```

2. web 查看
先做好端口映射，然后在50070端口查看 #todo


# 高可用Hadoop

前置：部署hadoop，zookeeper
高可用分为 [[HDFS]] 高可用以及，[[YARN]] 高可用。

## 集群安装
### 修改配置

1. 在 `core-site.xml`中添加 zookeeper 相关配置，集群地址，超时时长
```xml
<configuration>
    <property>
        <!-- ZooKeeper 集群的地址 -->
        <name>ha.zookeeper.quorum</name>
        <value>hadoop001:2181,hadoop002:2181,hadoop002:2181</value>
    </property>
    <property>
        <!-- ZKFC 连接到 ZooKeeper 超时时长 -->
        <name>ha.zookeeper.session-timeout.ms</name>
        <value>10000</value>
    </property>
</configuration>
```

2. 在 `hdfs-site.xml` 配置，逻辑名称与id列表，RPC通信地址，HTTP通信地址，Journalnode，隔离机制，ssh超时，访问代理，故障自动转移
```xml
<configuration>
    <property>
        <!-- 指定 HDFS 副本的数量 -->
        <name>dfs.replication</name>
        <value>3</value>
    </property>
    <property>
        <!-- 集群服务的逻辑名称 -->
        <name>dfs.nameservices</name>
        <value>mycluster</value>
    </property>
    <property>
        <!-- NameNode ID 列表-->
        <name>dfs.ha.namenodes.mycluster</name>
        <value>nn1,nn2</value>
    </property>
    <property>
        <!-- nn1 的 RPC 通信地址 -->
        <name>dfs.namenode.rpc-address.mycluster.nn1</name>
        <value>hadoop001:8020</value>
    </property>
    <property>
        <!-- nn2 的 RPC 通信地址 -->
        <name>dfs.namenode.rpc-address.mycluster.nn2</name>
        <value>hadoop002:8020</value>
    </property>
    <property>
        <!-- nn1 的 http 通信地址 -->
        <name>dfs.namenode.http-address.mycluster.nn1</name>
        <value>hadoop001:50070</value>
    </property>
    <property>
        <!-- nn2 的 http 通信地址 -->
        <name>dfs.namenode.http-address.mycluster.nn2</name>
        <value>hadoop002:50070</value>
    </property>
    <property>
        <!-- NameNode 元数据在 JournalNode 上的共享存储目录 -->
        <name>dfs.namenode.shared.edits.dir</name>
        <value>qjournal://hadoop001:8485;hadoop002:8485;hadoop003:8485/mycluster</value>
    </property>
    <property>
        <!-- Journal Edit Files 的存储目录 -->
        <name>dfs.journalnode.edits.dir</name>
        <value>/home/hadoop/journalnode/data</value>
    </property>
    <property>
        <!-- 配置隔离机制，确保在任何给定时间只有一个 NameNode 处于活动状态 -->
        <name>dfs.ha.fencing.methods</name>
        <value>sshfence</value>
    </property>
    <property>
        <!-- 使用 sshfence 机制时需要 ssh 免密登录 -->
        <name>dfs.ha.fencing.ssh.private-key-files</name>
        <value>/root/.ssh/id_rsa</value>
    </property>
    <property>
        <!-- SSH 超时时间 -->
        <name>dfs.ha.fencing.ssh.connect-timeout</name>
        <value>30000</value>
    </property>
    <property>
        <!-- 访问代理类，用于确定当前处于 Active 状态的 NameNode -->
        <name>dfs.client.failover.proxy.provider.mycluster</name>
<value>org.apache.hadoop.hdfs.server.namenode.ha.ConfiguredFailoverProxyProvider</value>
    </property>
    <property>
        <!-- 开启故障自动转移 -->
        <name>dfs.ha.automatic-failover.enabled</name>
        <value>true</value>
    </property>
</configuration>
```

3. 在 `yarn-site.xml`，启用 HA、 MA，RM 集群表示和id，RM服务地址，RM web地址，zk集群地址，启用自动回复，持久化存储类
```xml
<configuration>
    <property>
        <!-- 是否启用日志聚合 (可选) -->
        <name>yarn.log-aggregation-enable</name>
        <value>true</value>
    </property>
    <property>
        <!-- 聚合日志的保存时间 (可选) -->
        <name>yarn.log-aggregation.retain-seconds</name>
        <value>86400</value>
    </property>
    <property>
        <!-- 启用 RM HA -->
        <name>yarn.resourcemanager.ha.enabled</name>
        <value>true</value>
    </property>
    <property>
        <!-- RM 集群标识 -->
        <name>yarn.resourcemanager.cluster-id</name>
        <value>my-yarn-cluster</value>
    </property>
    <property>
        <!-- RM 的逻辑 ID 列表 -->
        <name>yarn.resourcemanager.ha.rm-ids</name>
        <value>rm1,rm2</value>
    </property>
    <property>
        <!-- RM1 的服务地址 -->
        <name>yarn.resourcemanager.hostname.rm1</name>
        <value>hadoop002</value>
    </property>
    <property>
        <!-- RM2 的服务地址 -->
        <name>yarn.resourcemanager.hostname.rm2</name>
        <value>hadoop003</value>
    </property>
    <property>
        <!-- RM1 Web 应用程序的地址 -->
        <name>yarn.resourcemanager.webapp.address.rm1</name>
        <value>hadoop002:8088</value>
    </property>
    <property>
        <!-- RM2 Web 应用程序的地址 -->
        <name>yarn.resourcemanager.webapp.address.rm2</name>
        <value>hadoop003:8088</value>
    </property>
    <property>
        <!-- ZooKeeper 集群的地址 -->
        <name>yarn.resourcemanager.zk-address</name>
        <value>hadoop001:2181,hadoop002:2181,hadoop003:2181</value>
    </property>
    <property>
        <!-- 启用自动恢复 -->
        <name>yarn.resourcemanager.recovery.enabled</name>
        <value>true</value>
    </property>
    <property>
        <!-- 用于进行持久化存储的类 -->
        <name>yarn.resourcemanager.store.class</name>
	    <value>org.apache.hadoop.yarn.server.resourcemanager.recovery.ZKRMStateStore</value>
    </property>
</configuration>
```

4. 在 `mapred-site.xml`
```xml
<configuration>
    <property>
        <!--指定 mapreduce 作业运行在 yarn 上-->
        <name>mapreduce.framework.name</name>
        <value>yarn</value>
    </property>
</configuration>
```

### 分发程序
将配置分发到各台服务器

## 启动集群

### 启动 ZK

分别到三台服务器上启动 ZooKeeper 服务：

``` shell
zkServer.sh start
```

### 启动 JN
分别到三台服务器下，启动 `journalnode` 进程：

```shell
hdfs --daemon start journalnode
```

### 初始化 NN
在 `hadop001` 上执行 `NameNode` 初始化命令：

```
hdfs namenode -format
```
执行初始化命令后，需要将 `NameNode` 元数据目录的内容，复制到其他未格式化的 `NameNode` 上。元数据存储目录就是我们在 `hdfs-site.xml` 中使用 `dfs.namenode.name.dir` 属性指定的目录。这里我们需要将其复制到 `hadoop002` 上：

```shell
 scp -r /home/hadoop/namenode/data hadoop002:/home/hadoop/namenode/
```

### 初始化 HA 状态
在任意一台 `NameNode` 上使用以下命令来初始化 ZooKeeper 中的 HA 状态：

```shell
hdfs zkfc -formatZK
```

### 启动 Hadoop

``` bash
# 配置环境变量
export HDFS_JOURNALNODE_USER=root
export HDFS_ZKFC_USER=root
```

启动

```shell
start-all.sh
```

需要注意的是，这个时候 `hadoop003` 上的 `ResourceManager` 服务通常是没有启动的，需要手动启动：

```shell
yarn --daemon start resourcemanager
```

问题
1. NodeManager， ResourceManager 没有启动

在 `etc/hadoop/yarn-env.sh` 添加配置
``` bash
export YARN_RESOURCEMANAGER_OPTS="--add-opens java.base/java.lang=ALL-UNNAMED"
export YARN_NODEMANAGER_OPTS="--add-opens java.base/java.lang=ALL-UNNAMED"
```

