Hadoop 下的分布式文件系统，具有高容错、高吞吐量等特性，可以部署在低成本的硬件上

## 架构

HDFS 遵循主/从架构，由单个 NameNode(NN) 和多个 DataNode(DN) 组成
- NN：负责执行有关**文件系统命名**空间的操作，例如打开，关闭、重命名文件和目录等。它同时还负责集群元数据的存储，**记录着文件中各个数据块的位置信息**
- DN：负责提供来自文件系统客户端的**读写请求，执行块的创建，删除等操作**

通过 NN 来确定 DN 的存活状态，同时客户端也通过 NN 来进行数据读取与写入。

```mermaid
flowchart LR
subgraph 集群
subgraph 机架1
n11[节点1]
n12[节点2]
n1x[节点...]
n1n[节点n]
n11 --- n12
n12 --- n1x
n1x --- n1n
end
subgraph 机架2
n21[节点1]
n22[节点2]
n2x[节点...]
n2n[节点n]
n21 --- n22
n22 --- n2x
n2x --- n2n
end
end
```


## 特点
* 简单一致性模型
HDFS 更适合于一次写入多次读取 (write-once-read-many) 的访问模型。支持将内容追加到文件末尾，但不支持数据的随机访问，不能从文件任意位置新增数据。
- 高吞吐量
HDFS 设计的重点是支持高吞吐量的数据访问，而不是低延迟的数据访问
- 高容错
- 跨平台
- 大文件支持
HDFS 将每一个文件存储为一系列块，块大小是 128M，针对数据处理也是面向块的。所以块越大，就更加方便处理大文件了。
hadoop 2.x默认块大小为128M. hadoop3.x默认块大小为256M。
建议设置块大小为接近你硬盘的输出速率的2的N次方。比如你的硬盘平均输出速率为200M/s.那么就设置成256M。如果是400M/s，就设置成512M



## 主备 NN 元数据同步机制
Hadoop 支持使用 Quorum Journal Manager (QJM) 或 Network File System (NFS) 作为共享的存储系统。

### QJM
思想来源于Paxos协议

主备两个节点需要保持一致性和可用性，提出了 JorunalNode 。JournalNode 节点作为主节点和备节点的中间节点，也是一个小集群，只要有过半的节点完成了数据的存储就算成功。

> [HDFS 在 HA 模式集群下 JournalNode 节点的作用](https://cloud.tencent.com/developer/article/1917651)