### 第一部分：开发基础，3～4周
java 数据结构+简单网络
jvm
分布式+复杂网络RPC，Netty + zookeeper

**10月15日截止**

### 第二部分：大数据框架，5～6周
hadoop
hive
spark
flink
hbase
kafka
相关算法

**11月26日截止**

### 第三部分：进阶实战，2～3周
搭建 HDFS+Yarn+Hive+Hbase+Azkaban+Flume+Kafka+Spark+Flink+Mysql+Redis 平台，并处理相关数据

**1月17日截止**