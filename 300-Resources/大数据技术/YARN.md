用户可以将各种服务框架部署在 YARN 上，由 YARN 进行统一地管理和资源分配。比如 HBase，Tez 等。
## 架构
---
### Resource Manager
通常在独立的机器上以后台进程的形式运行，它是整个集群资源的主要协调者和管理者。
（相当于总经理）
### Node Manager
是 YARN 集群中的每个具体节点的管理者。主要负责该节点内所有容器的生命周期的管理，监视资源和跟踪节点健康。
可以有多个。
（相当于部门经理）
### Application Manager
在用户提交一个应用程序时，YARN 会启动一个轻量级的进程 ApplicationMaster
（相当于执行者）
### Container
是 YARN 中的资源抽象，它封装了某个节点上的多维度资源，如内存、CPU、磁盘、网络等


作业直接提交到 YARN 上运行