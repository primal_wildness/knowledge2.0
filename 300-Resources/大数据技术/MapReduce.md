Hadoop MapReduce 是一个分布式计算框架，用于编写批处理应用程序。
是否能够尝试把使用场景看作SQL语句情景下？
## 流程

1. 读取文件数据
2. 拆分
3. 映射
4. 打乱：Shuffle阶段
5. 减并

MapReduce 编程模型中 `splitting` 和 `shuffing` 操作都是由框架实现的，需要我们自己编程实现的只有 `mapping` 和 `reducing`，这也就是 MapReduce 这个称呼的来源。
### Shuffle阶段
Hadoop的核心思想是MapReduce，但shuffle又是MapReduce的核心。shuffle的主要工作是从Map结束到Reduce开始之间的过程。shuffle阶段又可以分为Map端的shuffle和Reduce端的shuffle。
由于Shuffle涉及到了磁盘的读写和网络的传输，因此Shuffle性能的高低直接影响到了整个程序的运行效率

Map端shuffle：
首先把 map 的结果写入 `环形内存缓冲区`，并且进行分区。当写入数据量超过阈值后，将缓冲区数据 `溢出写` 到磁盘中，并根据键值进行排序与合并（combine，可选）。整个 map 任务完成 `溢出写` 后，进行归并生成最后结果。

```mermaid
flowchart LR
subgraph spill
style p1 fill:#FE4A49;
style p2 fill:#7EBDC2;
style p3 fill:#2A7F62;
p1
p2
p3
end
map --> spill
```

Reduce端的shuffle：
包括 copy、merge 和 reduce 三个阶段。
* reduce task从每个map task的结果文件中拉取对应分区的数据。
- reduce task从每个map task拉取分区数据的时候会进行再次合并，排序，按照自定义的reducer的逻辑代码去处理。
- reduce 产生结果。

```mermaid
flowchart LR
subgraph spilla
style pa1 fill:#FE4A49;
style pa2 fill:#7EBDC2;
style pa3 fill:#2A7F62;
pa1 
pa2
pa3
end
subgraph spillb
style pb1 fill:#FE4A49;
style pb2 fill:#7EBDC2;
style pb3 fill:#2A7F62;
pb1 
pb2
pb3
end

subgraph reduce
style p1 fill:#FE4A49;
style p2 fill:#7EBDC2;
style p3 fill:#2A7F62;
p1
p2
p3
end
pa1 --> p1
pb1 --> p1
pa2 --> p2
pb2 --> p2
pa3 --> p3
pb3 --> p3


```

为什么有两处排序？
mr 是全局有序，所以两个阶段都需要排序，前者是局部有序，后者是全局有序。

[MapReduce Shuffle 和 Spark Shuffle](https://mp.weixin.qq.com/s?__biz=MzUxOTU5Mjk2OA==&mid=2247485991&idx=1&sn=79c9370801739813b4a624ae6fa55d6c)

## Spark 中的 MR

[[Spark]] 的Shuffle是在MapReduce Shuffle基础上进行的调优。其实就是对排序、合并逻辑做了一些优化。

在Spark中Shuffle write相当于MapReduce 的map，Shuffle read相当于MapReduce 的reduce。

spark的shuffle分为两种实现，分别为HashShuffle和SortShuffle。

1. HashShuffle又分为普通机制和合并机制

2. SortShuffle也分为普通机制和bypass机制
每个Task在进行shuffle操作时，虽然也会产生较多的临时磁盘文件，但是最后会将所有的临时文件合并(merge)成一个磁盘文件，因此每个Task就只有一个磁盘文件。

不会进行排序
## 为什么使用 MR

1. 海量数据在单机上的限制。
	可以举个例子，比如 `m*n` 的数据量，单机复杂度为 `O(m*n)` ，而交给 m 台机器，复杂度将为 `O(n)` 。其中映射与合并的思想也能够减少冗余。

2. 可以将业务逻辑与分布式计算细节分开。
3. 这种结构能够减少网络传输？

[MapReduce架构](https://developer.aliyun.com/article/679395)
