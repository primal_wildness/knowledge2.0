### 步骤

1.  分析序列的平稳性
    
    ARIMA 模型对时间序列的要求是平稳型。因此，当你得到一个非平稳的时间序列时，首先要做的即是做时间序列的差分，直到得到一个平稳时间序列。如果你对时间序列做d次差分才能得到一个平稳序列，那么可以使用ARIMA(p,d,q)模型，其中d是差分次数。
    
2.  通过ACF和PACF的图选择出p值和q值
    
    具体要查表
    
    （截尾：落在置信区间内95%）
| 模型 | ACF | PACF |
| -- | -- | -- |
| AR（确定p） | 衰减趋于零 | p阶后截尾 |
| MA（确定q） | q阶后截尾 | 衰减趋于零 |
| ARMA（确定p，q） | 衰减趋于零 | 衰减趋于零 |

    如何确定p和q：
    
    确定q值：在ACF上q阶后截尾，且在PACF上q后面都趋向0，选定此q。
    
    确定p值：如上表
    
    -   如果可能有多组p和q，那么可以用BIC评估
3.  调用ARIMA模型：ARIMA（p，d，q）
    

### 平稳性分析

1.  可以看ACF和PACF图
    
    若自相关系数很快就衰减到0附近，是明显的平稳序列
    
    看PACF则可以通过阶数发现序列和前第几项有关。（比如类似股票的随机游走图，阶数为1时相关性很大，之后的阶数自相关性就很小，表示只和前面一项有关。又比如一些周期性的数据会和一些固定的阶数相关）
    
2.  简单统计法
    
    将序列分成前后两部分，比较两者均值和方差
    
3.  假设检验法
    
    -   DF检验
    -   ADF检验
    -   PP检验
    -   DF-GLS检验
    -   KPSS检验

> [](https://blog.csdn.net/Tw6cy6uKyDea86Z/article/details/121005440)[https://blog.csdn.net/Tw6cy6uKyDea86Z/article/details/121005440](https://blog.csdn.net/Tw6cy6uKyDea86Z/article/details/121005440)

> [](https://zhuanlan.zhihu.com/p/425664064)[https://zhuanlan.zhihu.com/p/425664064](https://zhuanlan.zhihu.com/p/425664064)

> 确定趋势 随机趋势 d阶单整：如果时间序列的阶差分为平稳过程，则称为 “d阶单整” (Integrated of order d) 单位根：单位根的n次，表示把复数圆分割成n个，从 1+0i 开始分割

$Y_t = \beta Y_{t-1} + \epsilon_t$

用代码理解就是，随机化数组元素，然后：arr[i] += b*arr[i-1]

### 额外工作

1.  对预测结果是否需要添加周期性信息

### 问题

-   为什么不能处理周期性数据？
    
    因为周期性信息会抵消，模型无法识别你的数据到底是哪种季节/周期特征