+++
title="SublimeText3使用"
date=2022-11-07
in_search_index = true

categories = ["工具使用"]
tags = ["文本编辑器"]
[extra]
toc = true
comments = false
+++

### 基础功能

编译运行：ctrl + shift +b

### 切换vim模式

- 开启与关闭

偏好设置里面的Vintage注释掉：

```python
"ignored_packages":
[
	//"Vintage"
],
```

- vim的键位冲突

sublime->preference->key binding 添加修改

但是无法解决ctrl+v的问题

- 插件

NeoVintageous