---
title: "项目"
date: 2023-06-13
---

项目名：nationwater

介绍：国家水质监测站数据自动收集与分析项目

状态：归档📁

链接：[传送门🚪](https://gitee.com/primal_wildness/nationwater)

---
项目名：Code-Assistants

介绍：一些减少重复劳动力，或者不舍得扔掉的代码工具集合

状态：维护中🚧

链接：[传送门🚪](https://github.com/gatzai/Code-Assistants)

---

项目名：湖南工商大学Latex模板

介绍：包含课程论文和硕士论文的Latex模板

状态：归档📁

链接：[传送门🚪](https://gitee.com/primal_wildness/hutb_latex)

---