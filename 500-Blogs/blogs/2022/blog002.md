+++
title="卡片笔记法"
date=2022-11-10

categories = ["笔记"]
tags = ["卡片笔记法"]

[extra]
toc = true
comments = true
+++

## 卡片盒笔记法

Zettelkasten

这个方法能够帮助我：

- 构建完备的知识网络
- 在需要时寻找的内容
- 找到有意义的连接
- 高效的开发创意和观点

## 做法

1. 摘要笔记
    
    阅读时做的笔记，适当的引用，用自己的话简要概括，方便后期寻找和想起
    
2. 引用笔记
    
    读完之后做的笔记，大致内容
    
3. 永久性笔记
    
    尝试将笔记和现有的想法联系起来。兴趣、学习、研究和思考之间找到联系。
    
    这并不是为了收集更多的笔记，而是为现有的想法、创意和观点增添新的价值。
    
    寻找`关键字`是非常重要的一步，这将和你现有的知识体系联系起来，因为知识是建立在连接之上的。不要给`关键字`分类，而是要想，我在什么时候会用到这个东西，一般会搜索什么，我们在什么时候，或者怎样才会触及到这个知识点。
    
    记录相关笔记。
    
    最后将这份笔记标记为`EverGreen`
    
4. 回顾
    
    提出问题，做出修改。