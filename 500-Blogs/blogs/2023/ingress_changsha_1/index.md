+++
title="Ingress 长沙任务补全计划之一「浣溪沙」"
date=2023-06-11
in_search_index = true
lastmod=2023-06-13
tags = ["ingress"]
[extra]
toc = true
show_comment = true
cc_license = true
+++

长沙任务之「浣溪沙」

<!--more-->

### 碎碎念
今天天气真的超热，不适合做任务，做完任务后感觉都被晒掉了一层皮。

<center class='half'>
    <img width='50%' src="https://cdnjson.com/images/2023/06/11/sun.jpg"/>
</center>

出师不利啊，作为攻略集合的开头，一个不小心就把任务做反了。正解应该是从第12个任务开始做。

### 出行表
推荐任务起始位置：南门口地铁站，1号口
用时：30min
方式：步行


### 注意事项
* 任务 \#10-6  的 Portal 「时尚名堂」需要安装模组，请确保能够执行。
* 任务从 \#12 开始，每个子任务的起始点顺序如下

<center class='half'>
    <img width='100%' src="https://cdnjson.com/images/2023/06/13/huanxisha.png"/>
</center>


### 任务问题
* 任务 \#3-3 : 在哪次事件中，火宫殿庙宇付之一炬？
	```txt
	文夕大火
	```
* 任务 \#3-4 : 截至2016年1月1日长沙市区共有多少个以毛泽东像为potal的？（请用阿拉伯数字表示）
	```txt
	5
	```
* 任务 \#3-5 : 截至2016年1月1日长沙市区其他以毛泽东像为名的portal中，哪个离该portal最近？哪个离该portal最远？（用、隔开）
	```txt
	青年毛泽东雕像、贤德二食堂毛泽东铜像
	```
* 任务 \#3-6 : 这是你做的第几个浣溪沙任务（用阿拉伯数字表示）
	```txt
	10
	```

### 线路图

路线范围大致如下，请忽略图中的起点和终点。

下次重做一遍补上🚧

### 友情连接

[Ingress长沙-目前为止遇到最难的问题](https://www.bilibili.com/video/BV1nX4y1C7g9/?spm_id_from=333.880.my_history.page.click&vd_source=be64fb00ff66079e029634bb4dd1f1bf)