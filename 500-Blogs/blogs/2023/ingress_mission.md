+++
title="Ingress 任务之「I Love Changsha」"
date=2023-02-18
in_search_index = true

tags = ["ingress"]

[extra]
toc = true
show_comment = true
cc_license = true
+++

本周六乘着有空，就打算做一个长沙地标打卡任务「I Love Changsha」。

<!--more-->
本任务足足有72块，可以说是我做过最长的任务。任务包括 hack，回答简单的问题。所有任务点都可达，但小心大量人流。

任务从早上10点开始，一直做到下午 3 点。除去休息和吃饭时间，任务总时长大概有 5 h。

明明嘴上说着算了，但是身体还是很诚实地做完了。总之，💙很有出息！
<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/m_changshabb75a3a22273f6ce.jpeg"/>
</center>

## 废话不多说，GO

早上9点多从学校出发，大概10点到达五一广场。本来还想着先嗦一碗粉，但也不饿，所以还是直接开始。从「五一广场地铁站」 ② 号口出来，任务第一个 po 是 「I Love CS」，注意同时这也是任务地最后一个 po，也就意味着你到后面不管多累，还是要回到这个点（分多次做完地就不用在意了）。

本任务路线其实和上次的差不多，行动范围甚至比上次的要小，但会在某个范围内逗留很长时间，主要是做多个任务和回答问题。

接下来一路就是 「太平老街」,「坡子街」,「黄兴广场」。

### 白果园巷

到了第一个停留点是「白果园巷」，这里是以前的富人区。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/p1c8c5ac2e8c4bf281.jpeg"/>
</center>
&emsp;
<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/p2a94ea84803dd549a.jpeg"/>
</center>

### 天心阁

离开「白果园巷」，一直往东走，就来到了老地方，也是第二个停留点「天心阁」。这次补了一张照片。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/k17e53f36f385e0a99.jpeg"/>
</center>

在这里会从30左右的任务一直做的40多的任务，而且后面还会再进来一次做60多的任务。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/k2963f98c146e5d7b1.jpeg"/>
</center>

随手拍路人，貌似有点歪

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/k334c286343401004d.jpeg"/>
</center>

### 古玩市场

走出「天心阁」，拐个弯，来到第三个停留点「古玩市场」，基本上就在马路古玩摊位前面逛街。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/g2540f72008d089059.jpeg"/>
</center>

从天心阁上面偷瞄摊位。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/g159b3d3a9a16dd255.jpeg"/>
</center>

做到 50 多个任务的时候，XM（物理）也基本耗尽了。这时大概一点半，所以是时候去「民间沙水」嗦一碗粉了。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/l21959f7c9877e9ecb.jpeg"/>
</center>

19 软妹币一碗的牛腩粉，小贵。很普通的扁粉，但牛腩还是很大块，只能说物有所值吧。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/l17e361c4506ff8760.jpeg"/>
</center>

最后就原路返回了，到了下午人变得多起来了。回到「太平老街」，瞬间就被人山人海给支配了。以至于进地铁站还要排队🙄。我真的是一步也走不动了。

<center class='half'>
    <img width='50%' src="https://img.picgo.net/2023/02/19/b1eb76a0a343f73bcc.jpeg"/>
</center>

## 附上线路图

![route](https://img.picgo.net/2023/02/19/routee2f6ea5b06e1c099.png)