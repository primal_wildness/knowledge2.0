+++
title="使用Bert预训练模型做下游任务"
date=2023-07-17

tags = ["bert", "nlp"]
+++
使用Bert预训练模型动手实践情感分类和阅读理解。

<!--more-->

大语言模型热火朝天，用天价的资源去训练语言模型肯定不是我们一般人能做的，而使用别人开发好的预训练模型来完成下游任务则是一条可行的道路，本人也想体验一下动手做语言模型下游任务的魅力。

> 阅读前注意：本文不会对 Bert 的原理做详细的解释说明，只是对下游任务的具体运行方式进行一个总结，方便不熟悉的人快速上手使用。

## 环境准备
---
### python 包的准备
```python
from transformers
from datasets
```

### 模型准备

模型和数据的获取就从开源社区 [Hugging face](https://huggingface.co/) 上下载。

> Hugging face 是一个开源社区，它提供了先进的NLP模型，数据集以及其他便利工具。在国内huggingface也是应用非常广泛，一些开源框架本质上就是调用transfomer上的模型进行微调。

本文选取的数据是中文的，所以下载中文相关的bert模型 [bert-base-chinese](https://huggingface.co/bert-base-chinese/tree/main)。
其内容如下：
```txt
下载内容如下：
1. config.json
2. pytorch_model.bin
3. tokenizer.json
4. vocab.txt

假设以上文件放在了路径 /home/bert-chinese/ 
那么加载方式如下：
pretrained = BertModel.from_pretrained('/home/bert-chinese/')
```

数据第一次使用时会自动下载，具体位置在用户目录的 .cache 文件中
```python
load_dataset(path='seamew/ChnSentiCorp', split=split)
```

### 电脑配置
本人配备一张显卡GTX960M，显存为2G。

## 大体思路
---
1. 定义数据集
2. 批处理加载器，同时对文本进行编码，转换为Bert能处理的形式
3. 加载Bert中文模型
4. 定义下游任务模型
5. 训练和测试下游任务模型

## 具体任务
---
### 情感分类
情感分类任务是判断给出的文本是积极情感还是消极情感，是一个二分类任务，


### 阅读理解
阅读理解任务是通过给出的上下文以及问题，预测答案在上下文出现的位置，所以是一个预测问题。