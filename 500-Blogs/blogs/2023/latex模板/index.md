+++
title="Latex 模板"

date=2023-05-24

tags = ["学术"]

+++

课程论文模板 + 硕士学位论文模板

<!--more-->

本文参考 [湖南工商大学研究生课程论文（模板）2023版](https://gra.hutb.edu.cn/p179/tzgg/20230509/145869.html) 以及 [湖南工商大学硕士学位论文（格式）2022版](https://gra.hutb.edu.cn/xwgl2/137582.jhtml) 两个模板，使用 [Overleaf](https://www.overleaf.com/) 生成 Latex 模板。

注意：其中《研究生课程论文评定》表格尚未放入模板中

## 说在前面
---
本仓库地址为：https://gitee.com/primal_wildness/hutb_latex
这可以说是我第一个小规模开放的仓库，今后的论文我也会尽可能使用这个模板，并持续改进这个仓库。

## 文件结构说明
---
主要内容在 text 目录下，包括封面，摘要，文章目录，正文章节，
图片资源在 figure 目录下，引用文献在 bibtext 目录下。
文档入口为 main.tex ，是导入相关章节的入口。

文件结构如下：
```tree
├── bib
│   └── bibtext.bib      % 引用文献
├── figures     % 图片资源
├── style         % 样式设置，导入相关包
├── text          % 主要内容
├── main.tex      % 文档入口
└── SimSun.ttc    % 字体文件，宋体
```