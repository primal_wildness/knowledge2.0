+++
title="长沙Ingress任务图片大集合"
date=2024-07-18
in_search_index = true

tags = ["ingress", "mission"]

[extra]
toc = true
show_comment = true
cc_license = true
+++

客官，这里有长沙大部分Ingress任务的缩略图哦~

<!--more-->

### 1.橘猫猫日也想喝茶颜
---
![橘猫猫日也想喝茶颜](https://api.bannergress.com/bnrs/pictures/00333032ef0a2e353b549909b8f1285e)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%A9%98%E7%8C%AB%E7%8C%AB%E6%97%A5%E4%B9%9F%E6%83%B3%E5%96%9D%E8%8C%B6%E9%A2%9C-5bd6)
### 2.#CSX-帆船运动
---
![#CSX-帆船运动](https://api.bannergress.com/bnrs/pictures/ac2dcdf0ac6b3426162032fa6368d031)
[🚪Bannergress传送门](https://bannergress.com/banner/csx-%E5%B8%86%E8%88%B9%E8%BF%90%E5%8A%A8-62bc)
### 3.EVA使徒系列
---
![EVA使徒系列](https://api.bannergress.com/bnrs/pictures/2cf264196ef89a1ae2c97cc12652ef03)
[🚪Bannergress传送门](https://bannergress.com/banner/eva%E4%BD%BF%E5%BE%92%E7%B3%BB%E5%88%97-8665)
### 4.#IngressSS-CSX
---
![#IngressSS-CSX](https://api.bannergress.com/bnrs/pictures/6d43e52e63406b1e386356af998386f4)
[🚪Bannergress传送门](https://bannergress.com/banner/ingressss-csx-ed79)
### 5.#CSX-CuteBunny
---
![#CSX-CuteBunny](https://api.bannergress.com/bnrs/pictures/98d9a9e1f6390fef9a71ab24dc5fb7ab)
[🚪Bannergress传送门](https://bannergress.com/banner/csx-cutebunny-0753)
### 6.麓山金秋
---
![麓山金秋](https://api.bannergress.com/bnrs/pictures/e570946997cc57b9059d914a05813160)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%BA%93%E5%B1%B1%E9%87%91%E7%A7%8B-1609)
### 7.杜甫
---
![杜甫](https://api.bannergress.com/bnrs/pictures/913296a02b7b4a0276c80a4b25613fe5)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%9D%9C%E7%94%AB-5e0c)
### 8.橘子洲头
---
![橘子洲头](https://api.bannergress.com/bnrs/pictures/942f8bcceb640f72e70ff21f1b871896)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%A9%98%E5%AD%90%E6%B4%B2%E5%A4%B4-fdfd)
### 9.湖大校徽
---
![湖大校徽](https://api.bannergress.com/bnrs/pictures/ffc363cb7d2f4c08b986c6e93c4751d5)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B9%96%E5%A4%A7%E6%A0%A1%E5%BE%BD-e914)
### 10.长沙理工大学校训
---
![长沙理工大学校训](https://api.bannergress.com/bnrs/pictures/b04d07adc39e06fa8eef19491e0d83fe)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E7%90%86%E5%B7%A5%E5%A4%A7%E5%AD%A6%E6%A0%A1%E8%AE%AD-5bf7)
### 11.长沙学院（原长沙大学）毕业纪念
---
![长沙学院（原长沙大学）毕业纪念](https://api.bannergress.com/bnrs/pictures/f3eac0fc6614153056789dc9bb88cded)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%AD%A6%E9%99%A2-%E5%8E%9F%E9%95%BF%E6%B2%99%E5%A4%A7%E5%AD%A6-%E6%AF%95%E4%B8%9A%E7%BA%AA%E5%BF%B5-6342)
### 12.Exploring the Beauty of CSUST
---
![Exploring the Beauty of CSUST](https://api.bannergress.com/bnrs/pictures/bc8c71743f14b15faacb8402e5ab19b4)
[🚪Bannergress传送门](https://bannergress.com/banner/exploring-the-beauty-of-csust-1fc4)
### 13.湖南商学院夜景
---
![湖南商学院夜景](https://api.bannergress.com/bnrs/pictures/2b3ac458cdc7ff8bba4ea882c4906e36)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B9%96%E5%8D%97%E5%95%86%E5%AD%A6%E9%99%A2%E5%A4%9C%E6%99%AF-4d2e)
### 14.鹿林之森
---
![鹿林之森](https://api.bannergress.com/bnrs/pictures/202fd0909be6f6011e58e0c200666705)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%B9%BF%E6%9E%97%E4%B9%8B%E6%A3%AE-0693)
### 15.星城长沙
---
![星城长沙](https://api.bannergress.com/bnrs/pictures/0a33a16ff228f1493fc3a988e3acf898)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%98%9F%E5%9F%8E%E9%95%BF%E6%B2%99-efd9)
### 16.这一世有喵星人和猫奴
---
![这一世有喵星人和猫奴](https://api.bannergress.com/bnrs/pictures/a1c8ad56fe55fb78545cefba11e1e196)
[🚪Bannergress传送门](https://bannergress.com/banner/%E8%BF%99%E4%B8%80%E4%B8%96%E6%9C%89%E5%96%B5%E6%98%9F%E4%BA%BA%E5%92%8C%E7%8C%AB%E5%A5%B4-82d7)
### 17.湘江江景
---
![湘江江景](https://api.bannergress.com/bnrs/pictures/0a0f903307aac34905387735ffe66bc3)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B9%98%E6%B1%9F%E6%B1%9F%E6%99%AF-c03e)
### 18.橘洲焰火
---
![橘洲焰火](https://api.bannergress.com/bnrs/pictures/160f6761ce0059ba79fa41be0626719e)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%A9%98%E6%B4%B2%E7%84%B0%E7%81%AB-a697)
### 19.笨柴兄弟
---
![笨柴兄弟](https://api.bannergress.com/bnrs/pictures/d2b3313a7d86b259ec30809d28ff7a78)
[🚪Bannergress传送门](https://bannergress.com/banner/%E7%AC%A8%E6%9F%B4%E5%85%84%E5%BC%9F-cd7f)
### 20.忍别离
---
![忍别离](https://api.bannergress.com/bnrs/pictures/3920625b996859f44269d1fbfa719019)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%BF%8D%E5%88%AB%E7%A6%BB-525f)
### 21.My Rock List
---
![My Rock List](https://api.bannergress.com/bnrs/pictures/afa8d8078b9578310236c9a93cb123ce)
[🚪Bannergress传送门](https://bannergress.com/banner/my-rock-list-3292)
### 22.贺龙体育场
---
![贺龙体育场](https://api.bannergress.com/bnrs/pictures/608a4474948b5dfc4f5b6789e24f7d01)
[🚪Bannergress传送门](https://bannergress.com/banner/%E8%B4%BA%E9%BE%99%E4%BD%93%E8%82%B2%E5%9C%BA-f5aa)
### 23.岳麓山
---
![岳麓山](https://api.bannergress.com/bnrs/pictures/647328c6afd3dac519f2190ff1fa6ea9)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%B2%B3%E9%BA%93%E5%B1%B1-8912)
### 24.天心阁
---
![天心阁](https://api.bannergress.com/bnrs/pictures/823c6b027d3184e9d3686ab17bb15f3f)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%A4%A9%E5%BF%83%E9%98%81-0ac7)
### 25.《浣溪沙》
---
![《浣溪沙》](https://api.bannergress.com/bnrs/pictures/88a78657bb84495af6c04761fa792695)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B5%A3%E6%BA%AA%E6%B2%99-f545)
### 26.印象浏阳
---
![印象浏阳](https://api.bannergress.com/bnrs/pictures/387005096935a76d3c173ee3c553372b)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%8D%B0%E8%B1%A1%E6%B5%8F%E9%98%B3-3a77)
### 27.星城长沙
---
![星城长沙](https://api.bannergress.com/bnrs/pictures/0681b12dcda3e321861b93265407cbe8)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%98%9F%E5%9F%8E%E9%95%BF%E6%B2%99-e054)
### 28.长沙天际线
---
![长沙天际线](https://api.bannergress.com/bnrs/pictures/86f9fff54583b0c5661481667687db86)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%A4%A9%E9%99%85%E7%BA%BF-68c3)
### 29.I Love Changsha
---
![I Love Changsha](https://api.bannergress.com/bnrs/pictures/4efc203c3f3e0af21d2e9fcbad960feb)
[🚪Bannergress传送门](https://bannergress.com/banner/i-love-changsha-1c9b)
### 30.#IngressFS Changsha Prime版1.0
---
![#IngressFS Changsha Prime版1.0](https://api.bannergress.com/bnrs/pictures/cb113062ff90ceed2fcf4efc24c2606c)
[🚪Bannergress传送门](https://bannergress.com/banner/ingressfs-changsha-prime%E7%89%881-0-dc0f)
### 31.#IngressFS 六一来长沙
---
![#IngressFS 六一来长沙](https://api.bannergress.com/bnrs/pictures/68d2b1132c89eb9e0eaaeddbb65ba786)
[🚪Bannergress传送门](https://bannergress.com/banner/ingressfs-%E5%85%AD%E4%B8%80%E6%9D%A5%E9%95%BF%E6%B2%99-1c99)
### 32.长沙网红打卡墙
---
![长沙网红打卡墙](https://api.bannergress.com/bnrs/pictures/cccd6ddc2e1f1bd0a5cf04d3006e9d9f)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E7%BD%91%E7%BA%A2%E6%89%93%E5%8D%A1%E5%A2%99-95f5)
### 33.长沙精神-乱搞版
---
![长沙精神-乱搞版](https://api.bannergress.com/bnrs/pictures/344470d484b2b17e1df9c3fc60462e57)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E7%B2%BE%E7%A5%9E-%E4%B9%B1%E6%90%9E%E7%89%88-8e80)
### 34.长沙美食
---
![长沙美食](https://api.bannergress.com/bnrs/pictures/599a612fba5b2ffb65da341c04453e7c)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E7%BE%8E%E9%A3%9F-4376)
### 35.长沙地铁一号线
---
![长沙地铁一号线](https://api.bannergress.com/bnrs/pictures/0a81bf0dd5f47e309f010e3979dbf470)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%9C%B0%E9%93%81%E4%B8%80%E5%8F%B7%E7%BA%BF-686b)
### 36.长沙地铁二号线
---
![长沙地铁二号线](https://api.bannergress.com/bnrs/pictures/4851c5c79867ad8cc89aaf5384e7864f)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%9C%B0%E9%93%81%E4%BA%8C%E5%8F%B7%E7%BA%BF-5f62)
### 37.长沙地铁三号线
---
![长沙地铁三号线](https://api.bannergress.com/bnrs/pictures/21bdbb070274094494f3dd5232030231)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%9C%B0%E9%93%81%E4%B8%89%E5%8F%B7%E7%BA%BF-82ec)
### 38.长沙地铁四号线
---
![长沙地铁四号线](https://api.bannergress.com/bnrs/pictures/24bc32d7dcbb565b6d12c24b9eb51a5f)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%9C%B0%E9%93%81%E5%9B%9B%E5%8F%B7%E7%BA%BF-f604)
### 39.长沙地铁五号线
---
![长沙地铁五号线](https://api.bannergress.com/bnrs/pictures/ecd1b39917d5dc0633a4abe3aad85696)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%9C%B0%E9%93%81%E4%BA%94%E5%8F%B7%E7%BA%BF-194d)
### 40.长沙地铁六号线
---
![长沙地铁六号线](https://api.bannergress.com/bnrs/pictures/d6ac587feb5e1bddef49ec2dbe220f53)
[🚪Bannergress传送门](https://bannergress.com/banner/%E9%95%BF%E6%B2%99%E5%9C%B0%E9%93%81%E5%85%AD%E5%8F%B7%E7%BA%BF-4bdd)
### 41.排排坐吃果果
---
![排排坐吃果果](https://api.bannergress.com/bnrs/pictures/1603403520d06a5ffcc0524753fe3a3d)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%8E%92%E6%8E%92%E5%9D%90%E5%90%83%E6%9E%9C%E6%9E%9C-22f8)
### 42.踏春
---
![踏春 ](https://api.bannergress.com/bnrs/pictures/29b4a9f974520d1d58aa925fa27fb8ce)
[🚪Bannergress传送门](https://bannergress.com/banner/%E8%B8%8F%E6%98%A5-a4eb)
### 43.#CSX Happy Mice
---
![#CSX Happy Mice](https://api.bannergress.com/bnrs/pictures/598649ede0ccbcc4380b7d520feaf4b9)
[🚪Bannergress传送门](https://bannergress.com/banner/csx-happy-mice-1b9e)
### 44.時雨ノ旅路vol.1
---
![時雨ノ旅路vol.1](https://api.bannergress.com/bnrs/pictures/dec80740a739aac8d72c62a8545cef67)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%99%82%E9%9B%A8%E3%83%8E%E6%97%85%E8%B7%AFvol-1-82fc)
### 45.夏目
---
![夏目](https://api.bannergress.com/bnrs/pictures/8025564eaa58f9d3941d811eca18a809)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%A4%8F%E7%9B%AE-b193)
### 46.湖南农大之春·樱
---
![湖南农大之春·樱](https://api.bannergress.com/bnrs/pictures/d664a39a4a2c522af64a3e7f031635bf)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B9%96%E5%8D%97%E5%86%9C%E5%A4%A7%E4%B9%8B%E6%98%A5-%E6%A8%B1-63a1)
### 47.乡里别想进城
---
![乡里别想进城](https://api.bannergress.com/bnrs/pictures/5535589ad3312f08987d78319f6dfc08)
[🚪Bannergress送门](https://bannergress.com/banner/%E4%B9%A1%E9%87%8C%E5%88%AB%E6%83%B3%E8%BF%9B%E5%9F%8E-2197)
### 48.猫和猫
---
![猫和猫 ](https://api.bannergress.com/bnrs/pictures/038b34e9e62ae7763274b0641323c767)
[🚪Bannergress传送门](https://bannergress.com/banner/%E7%8C%AB%E5%92%8C%E7%8C%AB-d242)
### 49.汉代瓦当
---
![汉代瓦当](https://api.bannergress.com/bnrs/pictures/fa460729276eac6c50b1a12c0500d90c)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B1%89%E4%BB%A3%E7%93%A6%E5%BD%93-bdf2)
### 50.这是客栈呀
---
![这是客栈呀](https://api.bannergress.com/bnrs/pictures/6cea33c98a16aec23655479c79a7cce6)
[🚪Bannergress传送门](https://bannergress.com/banner/%E8%BF%99%E6%98%AF%E5%AE%A2%E6%A0%88%E5%91%80-0dd1)
### 51.差不多是条废咸鱼了
---
![差不多是条废咸鱼了](https://api.bannergress.com/bnrs/pictures/d1b9163318b12f1fea6191a525edad22)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%B7%AE%E4%B8%8D%E5%A4%9A%E6%98%AF%E6%9D%A1%E5%BA%9F%E5%92%B8%E9%B1%BC%E4%BA%86-7493)
### 52.千年学府
---
![千年学府](https://api.bannergress.com/bnrs/pictures/2270bec1ad11a816bc6244284b21c7d7)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%8D%83%E5%B9%B4%E5%AD%A6%E5%BA%9C-dffa)
### 53.悠哉悠哉公园系列
---
![悠哉悠哉公园系列](https://api.bannergress.com/bnrs/pictures/8c982cb9b38eef106ef7499872e74415)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%82%A0%E5%93%89%E6%82%A0%E5%93%89%E5%85%AC%E5%9B%AD%E7%B3%BB%E5%88%97-239f)
### 54.冰与火之歌家族动物
---
![冰与火之歌家族动物](https://api.bannergress.com/bnrs/pictures/784311a5fdc99f4f051d7d3f2b097a18)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%86%B0%E4%B8%8E%E7%81%AB%E4%B9%8B%E6%AD%8C%E5%AE%B6%E6%97%8F%E5%8A%A8%E7%89%A9-94d1)
### 55.全是头头头
---
![全是头头头](https://api.bannergress.com/bnrs/pictures/22158a87f5ae63035978ffdedc9dcbc1)
[🚪Bannergress传送门](https://bannergress.com/banner/%E5%85%A8%E6%98%AF%E5%A4%B4%E5%A4%B4%E5%A4%B4-2297)
### 56.来到中南大学，实现人生理想
---
![来到中南大学，实现人生理想](https://api.bannergress.com/bnrs/pictures/65cef4c8d7ddc77a6b057a71046015fd)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%9D%A5%E5%88%B0%E4%B8%AD%E5%8D%97%E5%A4%A7%E5%AD%A6-%E5%AE%9E%E7%8E%B0%E4%BA%BA%E7%94%9F%E7%90%86%E6%83%B3-9b70)
### 57.Inside the Lanxiu City
---
![Inside the Lanxiu City](https://api.bannergress.com/bnrs/pictures/fe44e5caeddb907ae0a32a89f7ebb630)
[🚪Bannergress传送门](https://bannergress.com/banner/inside-the-lanxiu-city-3b52)
### 58.湖南农业大学校徽
---
![湖南农业大学校徽](https://api.bannergress.com/bnrs/pictures/86cade8c7339b7eb29222b1382e47fd9)
[🚪Bannergress传送门](https://bannergress.com/banner/%E6%B9%96%E5%8D%97%E5%86%9C%E4%B8%9A%E5%A4%A7%E5%AD%A6%E6%A0%A1%E5%BE%BD-2701)
### 59.荏苒冬春去
---
![荏苒冬春去](https://api.bannergress.com/bnrs/pictures/c9ae76cc19c1c095436bc79bf21bf71d)
[🚪Bannergress传送门](https://bannergress.com/banner/%E8%8D%8F%E8%8B%92%E5%86%AC%E6%98%A5%E5%8E%BB-9b6b)