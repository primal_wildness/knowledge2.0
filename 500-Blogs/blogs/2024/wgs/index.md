+++
title="武功山游记"
date=2024-07-25
in_search_index = true

tags = ["旅行" ]

[extra]
toc = true
show_comment = true
cc_license = true
+++

因为山就在那里

<!--more-->

### 夜爬
---
坐着最后一班高铁来到萍乡，将近晚上10点了，现场买了坐到武功山的大巴票，一小时的车程大家有说有笑，每个人都兴奋地像个小孩。

大约晚上十一点，我们穿过了黑色的夜，来到了山脚下，开启了夜爬。

<center>
<img width='60%' value="action" src = "https://img.picgo.net/2024/07/25/wgs_door725e652bc6e10f67.jpg"/>
</center>

晚上的人不算少，基本上前后都有三五成群的人，而且大部分都是年轻人。上山的路线非常完善，一路到山顶都是楼梯，楼梯两侧都有灯光照路，只要视野那边的尽头还是楼梯，你就需要一直往上爬。

随着海拔上升，能感受到一丝凉意，山上的风开始呼啸。

峰回路转，树木逐渐稀少，夜色中隐隐约约勾勒出巨大的山峰，有一种巨人在凝视你的感觉。

大约晚上三点，来到了一块平地，终于摆脱了楼梯的噩梦，这里视野非常开阔，在黑夜之中能听到草地随着风儿舞动的声音。

但此时距离金顶还有好几个山坡，我踏着云海，步入星辰，在凌烈狂风之中，继续翻山越岭。

远远望去，山那边露营地在黑夜中灯火通明，云雾缭绕，好似天上人间一般。

身体已经筋疲力尽了，只能走走停停，不时还得吃点东西补充体力。

走在山脊上往向下看，地面的道路流光溢彩，尽是人间繁华。

<center class='half'>
	<img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_night10fd80a5fdd28388a.jpg"/>
    <img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_night2344d49a9ec493969.jpg"/>
</center>

四点多终于登上了金顶，山顶上的人比我想象中的还要多，大家都围绕着“武功山”的石头拍照。

<center>
<img width='40%' value="action" src = "https://img.picgo.net/2024/07/25/wgs_top2e0040cf68878c7ed.jpg"/>
</center>


离开了人群，我才真正感受到山顶的寒冷，一件短袖加一件冲锋衣还是不足以保暖，虽然背包里面还有一件毛绒衣，但是我现在只想休息，也懒得拿出来了。

<center class='half'>
	<img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_top3df21b9d1b1fc8ed2.jpg"/>
    <img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_top109b8957dcf1a8a70.jpg"/>
</center>

我在山顶上闲逛，休息，然后找一个好的角度，蜷缩在楼梯旁等待日出。

### 日出
---
天空一开始是黑蓝色的，随着白昼增多，黑夜褪去，天空慢慢破晓。

橘黄色的光在云中若隐若现，像是戏剧舞台工作者在幕后调整灯光，云雾则是舞台前的小淘气。

开始时，只是淡淡的几笔色彩，等到朝阳露出头来，金光穿破了画布，这一刻，海阔天空。

不一会儿，整片大地都染上了太阳的温暖，驱散了所有的寒冷，温度也变得适宜。

![wgs sunrise](https://img.picgo.net/2024/07/25/wgs_sunrise7fd80bcab6f5bddd.jpg)

### 清晨
---
太阳绝对是一位非凡的画师

刚登顶时，凌晨，周围一片朦胧，阴沉

太阳画笔一挥，万物都显露出无与伦比的美丽

夜间沙沙作响的草坪此刻显露出一望无际的绿野，湿冷的雾气转变为厚实壮丽的云朵，黑洞洞的天空经过了绚烂的渐变，把周围都填充成一片蔚蓝

![wgs morning](https://img.picgo.net/2024/07/25/wgs_morning6d7db97db5694379.jpg)

![wgs wind1](https://img.picgo.net/2024/07/25/wgs_wind1d94a6702b96c8a0b.jpg)
![wgs wind2](https://img.picgo.net/2024/07/25/wgs_wind2da308f178e7e2bcf.jpg)
![wgs wind3](https://img.picgo.net/2024/07/25/wgs_wind3a38dbd3f03779fc5.jpg)

我真的想一直停留在山上，这里的景色让人百看不厌，广阔的绿地尽收眼底，视野之内铺满了草地。山坡缓缓下降，把整个视野拉的非常深，视觉给我带了的震撼，通感到全身，让我觉得身体轻飘飘的，变成云朵一般，躲在群山的襁褓之中。

<center class='half'>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_green215d7d35b996cae65.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_green1f89237c330081c76.jpg"/><img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_green487edd118b48e3607.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_green71137ab03d52b10dc.jpg"/>
</center>

<center class='half'>
	<img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green31f8c53498a9b4d16.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green5c5b5b144f9cbb23f.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green66c17141ea58500af.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green89e7a1211305350e5.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green960879053824d4756.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green102d7162c0831ac967.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green11316b4dfa1b74a79c.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_green12fed3e9d9690345a3.jpg"/>
</center>

虽然一整晚都没有睡觉，但是此时的我却没有丝毫困意，沿着山脊一直走，走到了反穿路线，身体仿佛不断从周边的美景吸收无穷的能量。山上的景色千变万化，时而阳光明媚，时而云雾缭绕，这种景色，仅仅依靠图片或者视频是无法诉说的，只有我们亲身经历才知道，人与自然是存在着深刻的连接。

<center class='half'>
	<img style="display: inline-block" width='90%' src="https://img.picgo.net/2024/07/25/wgs_rev3d89c42421b04c265.jpg"/>
	<img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_rev15b77de2ea7d75f5d.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_rev21132b17cc1b2c26d.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_rev77891912753397995.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_rev942b8e647069653ea.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_rev1111ecf8d7f233b672.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_rev12977611f5c8df8025.jpg"/>
</center>

山，真的是一种很神奇的自然景观，由土堆和石块不断堆叠起来，但是却造就了各种不朽的美景。它将横跨大地的四季浓缩在一片土地上，并往上延申；它集中了不同习性的动物，让它们能在同一片土地上交流；它以巨大的身姿，为万物搭上了通向天空的楼梯。

<center class='half'>
	<img style="display: inline-block" width='80%' src="https://img.picgo.net/2024/07/25/wgs_rev86d1c315dcfa97006.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_rev49fdff72658fae623.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_rev1060e8ca00624364c2.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_rev6e85c5b299e45cde4.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/wgs_rev51f1fa80cc181aa47.jpg"/>
</center>

### 下山
---
山顶美好的体验让我念念不舍，流连忘返。

<center class='half'>
	<img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down1bcaf8a13fc27adfc.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down2c524d9137d026b0a.jpg"/>
	<img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down3a7769918ccfde1ca.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down4eb9aebbac92ea437.jpg"/>	
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down50eea1b2ca4aff86c.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down71625361172ebfa20.jpg"/>
	<img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down87512c96d00671786.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down9e93e4b8231a60070.jpg"/>
    <img style="display: inline-block" width='30%' src="https://img.picgo.net/2024/07/25/wgs_down11ac9f9098763ce6b3.jpg"/>
    <img style="display: inline-block" width='60%' src=""/>
</center>

<center class='half'>
    <img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_down6183eb813973ac867.jpg"/>
	 <img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_down10128c16dec7f44964.jpg"/>
</center>

<center class='half'>
    <img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/wgs_down1261cc58890bdafa36.jpg"/>
</center>

峰回路转，又到了登山的地方。

我想，下次来就是武功山反穿之旅了。