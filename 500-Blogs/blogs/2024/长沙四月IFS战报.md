+++
title="蓝绿合作，AP多多"
date=2024-04-08
in_search_index = true

tags = ["ingress", "ifs"]

[extra]
toc = true
show_comment = true
cc_license = true
+++

四月，清明时节，长沙，乌云压城。湘江周围的 XM 能量不断涌现出来，形成两道细长的蓝绿裂缝，各位 agent 都纷纷前往长沙一探究竟。这未知能量的深渊中，蕴含了怎样的秘密？

<!--more-->
## 开冲
---

下表为本次参与长沙 IFS 的10位小伙伴，欢迎@BabyCheer和@zombieP378两位远道而来的 agent，同时也感谢@Yuwan2333，@aplumMan 以及@Mwish 的 RSVP。

~~喜报！ 四月长沙 IFS 是一个历史性扭转时刻，蓝军RSVP数量首次超过绿军，现场两队人数实现首次持平。~~

| **Enlighten**  | **Resistance**         |
| -------------- | ---------------------- |
| <u>@svnerd</u> | <u>@primalwildness</u> |
| @monsoonlh     | @XXXXssss              |
| @mContext      | @BabyCheer             |
| @hanaxilo      | @zombieP378            |
| @Oliverpi      | @Cryolitia             |
先来大合照，特地把湘绿的旗子拿出来了（终于正式亮相了）

![1together99a3d0e369beed07.jpg](https://cdnjson.com/images/2024/04/08/1together99a3d0e369beed07.jpg)

在签到介绍中写了 `祝大家武运隆昌` ，被误以为是武汉的 IFS 了，这下不得不征集一个专属于长沙 IFS 的成语了（征集采用报酬为 1 个 ADA + 1 个 JARVIS）（认真脸）

这次 IFS 举行地点依旧是在长沙 IFS 御用地 —— 太平街的茶颜悦色，感谢 @svnerd 提供源源不断的8炸😘，~~再顺便偷偷 diss 一下迟到的摄政王 @monsoonlh~~。

<center class='half'>
	<img style="display: inline-block" width='30%' src="https://cdnjson.com/images/2024/04/08/Screenshot_2024-04-08-22-40-31-88_948cd9899890cbd5c2798760b2b9537772273c7678069827.jpg"/>
    <img style="display: inline-block" width='30%' src="https://cdnjson.com/images/2024/04/08/Screenshot_2024-04-08-22-40-08-08_948cd9899890cbd5c2798760b2b953774b930c1e7ddf6ef5.jpg"/>
</center>

## 特别活动 — 蓝绿合作连多重
---

希望在 IFS 这天增加一些特别活动，开发出一些新的合作或者对抗的玩法，提升活动的趣味性。本着这样的想法，这次的 leader 们给大家举行了一个蓝绿合作连多重的小活动。

玩法如下图所示：
![spec886904b178600dd1.png](https://cdnjson.com/images/2024/04/08/spec886904b178600dd1.png)

活动前由 leader 凑齐相应数量的 key，并连好 Field（确保在活动 po 放 8 炸不会炸到两个 Field 的顶点），当天活动地点的两个 po 在如图的两个 Field 里面，蓝绿各一个 Field。各位参加活动的 agent 在这两个 po 上插脚、连多重，炸 po 活动。

每位 agent 有 45 把 key，每个 portal 有 15 把钥匙。每次往三个顶点进行连接，然后由对面阵营炸掉，连完后总体算下来每人大约有 7w+ AP。

<center>
<img width='60%' value="action" src = "https://cdnjson.com/images/2024/04/08/act158f7b61effa04470.jpg"/>
</center>

当然，这种玩法的缺点还是有的。首先是双方人数得持平，其次就是需要双方掌握好节奏。

总之希望大家 AP 多多，在 IFS 中体验到线下多人游戏的快乐😆。

## 解密 — 自定义解密路线
---

（每月一个小技巧，根本学不完！）

在申请 IFS 的表单时，阅读[指南文档](https://fevgames.net/ingressfs/org-guide/)时发现一个解密 po 的申请小技巧，即可以自定义解密路线。

一般来说，解密任务是在两小时内对 11 个 portal 进行 hack，并利用 hack 出来的媒体合成密码。至于是哪 11个 portal，可以分为以下两种情况。

**第一个**是猩猩会随机沿着某条路径选 11 个 portal。

**第二个**是自定义这 11 个 portal。具体的操作方法如下：

1. 首先打开 IITC，把已有的画图内容清除掉（点击小垃圾筒图标，然后点击clear all）。
2. 使用绘图工具绘制线条，自行选择 11 个解密 po 进行绘制，绘制顺序和解密顺序一样。
3. 随便点击一个 po， 打开详细信息页（左下角），并点击 DrawTools Opt，在打开的页面中点击 Copy Drawn Items 按钮，复制信息。
4. 将复制出来的信息写入申请表中

最后，不管是用哪种方法，在活动开始前，官方会把这 11 个 po 的照片发到 leader 的邮箱中。

## 开饭 — 不吃辣的也能来
---

在「鲁哥饭店」开饭，不辣的菜也有很多，符合来自不同地方 agent 的口味。

咸蛋黄茄子我觉得好吃呢，不过听各位 agent 说咸蛋黄土豆更好吃，没有茄子那么多油，这下不得不去试一试了。

本次最差评的菜可以说是卤牛肉炒辣椒了，被评价为辣椒炒辣椒，几乎没有肉。

其他菜感觉还行，不过有的菜还是太小碗了。不能吃辣的北雁竟然觉得辣菜不辣，酸辣腰子给她吃的嘛香嘛香。

最后人均 50 ¥，非常实惠。

<center class='half'>
    <img style="display: inline-block" width='52%' src="https://cdnjson.com/images/2024/04/08/food10245b3199f5bbfd0.jpg"/>
    <img style="display: inline-block" width='30%' src="https://cdnjson.com/images/2024/04/08/food2b4bf6ef881753701.jpg"/>
</center>

<center>
<img width='80%' value="action" src = "https://cdnjson.com/images/2024/04/08/profile6c22bbce5ddd4af5.jpg"/>
</center>

四月完结散花🌸🌺

```python
+----------------------------------------+
|               期待五月IFS               |
+-----------+---------------+------------+
|                                        |
|       欢迎大家来参加长沙五月的IFS😘      |
|                                        |
+-----------+---------------+------------+
```