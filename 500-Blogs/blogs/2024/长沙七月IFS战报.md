+++
title="长佛之约，相聚七月"
date=2024-07-14
in_search_index = true

tags = ["ingress", "ifs"]

[extra]
toc = true
show_comment = true
cc_license = true
+++

七月夏日炎炎，侦测到一大波 Agent 信号汇聚星城长沙！

<!--more-->
## 长佛之约
---

本次一共有 18 位 agent 参加了长沙的 IFS，真滴好多人呀。欢迎来自佛山旅游团，以及突然出现的野生Agent ，这次 IFS 举行地点依旧是在长沙 IFS 御用地 —— 太平街的茶颜悦色。

| **Enlighten**  | **Resistance**   |
| -------------- | ---------------- |
| <u>@svnerd</u> | <u>@XXXXssss</u> |
| @monsoonlh     | @primalwildness  |
| @mContext      | @ETO2HCN         |
| @hanaxilo      | @Kairo12         |
| @Oliverpi      | @Mwish           |
| @Hlxxxxxx      |                  |
| @ROGnvrwo      |                  |
| @josameer      |                  |
| @rabbitorange  |                  |
| @DivinerLee    |                  |
| @Mowa          |                  |
| @lysion        |                  |
| @YogaLee17     |                  |

按照惯例，先来一个大合照：早鸟交差版，亮出了酷酷的佛山旗子 && 迟到补拍版
![t1.jpg](https://cdnjson.com/images/2024/07/11/t1.jpg)
![t2.jpg](https://cdnjson.com/images/2024/07/11/t2.jpg)

这次又收到了大家的biocard，非常开森
<center>
<img width='60%' value="action" src = "https://img.picgo.net/2024/07/25/biocardf5bc055dece8a92e.jpg"/>
</center>


## 相聚茶颜
---
人未来，旗先到，Flag 就先立在这里了。不得不说这个旗子是真的不错，在人流量大的地方，你就是整条街最靓的仔/女，所有迷路的agent都会跟着你，甚至还可能吸引到野生agent（抱有一丝丝期望），还有就是多人聚集也不怕被保安注意到了，简直完美😎

<center>
<img width='30%' value="action" src = "https://img.picgo.net/2024/07/25/flag245f44d4f31f36ed5.jpg"/>
</center>

在长沙也能画圈辣！真是值得纪念的时刻，希望8月份也能继续（疯狂暗示，结尾有惊喜）

![iitc.jpg](https://img.picgo.net/2024/07/25/iitc9b66865008c4abf2.jpg)

### 折纸

来玩折纸！只需要买一杯茶颜，就能拿一张折纸（不买也行吧😏），按照说明把蝴蝶折出来，可以获得可爱小礼物。我们这么多人，拿个十张八张也不过分吧。
<center class='half'>
	<img style="display: inline-block" width='22%' src="https://img.picgo.net/2024/07/25/butterfly37b6975c43f19f37.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/cardafcbaef5120cf276.jpg"/>
    <img style="display: inline-block" width='22%' src="https://img.picgo.net/2024/07/25/giftd9f49f40875096cc.jpg"/>
</center>

### 音游大佬

IFS 正在慢慢成为音游佬的战斗圣地，包括但不仅限于打mai，Arcaea（又称源神，没用的知识又增加了）等等。我都不敢想象他们在一起打音游会有多快乐，此处 @Cryolitia，@zombieP378

## 开饭
---

在「笨萝卜」开饭，辛苦`油咖喱`帮大家排号，没想到人数太多了一张大桌还不够用。（人多了注意的事情就变多了，IFS是这样的，Leader 只需要点一个签到按钮，而其他 agent 考虑的事情就很多了（乱套公式））排队真的要排好久。
![eat.jpg](https://img.picgo.net/2024/07/25/eat2e808a6733f5a514.jpg)

好味好味，但是被辣麻了。最后人均 41 ¥，非常实惠。

![food.jpg](https://img.picgo.net/2024/07/25/food4a555f7a22cf20f7.jpg)


## 饭后活动
---
### 搞酒

尝鲜，麻瓜黄油啤酒好喝，黄油味很香，但是有点腻。
<center class='half'>
	<img style="display: inline-block" width='54%' src="https://img.picgo.net/2024/07/25/bear132728a1b7612af01.jpg"/>
    <img style="display: inline-block" width='40%' src="https://img.picgo.net/2024/07/25/bear2c1306d63dd80a61e.jpg"/>
</center>

### 任务

跟随本地 agent，长沙IFS老年旅游团带你打卡长沙任务
![mission.jpg](https://img.picgo.net/2024/07/25/missionbbfbb38bf11b23d1.jpg)
![csflag.jpg](https://img.picgo.net/2024/07/25/csflag1be4f2b4d638564e.jpg)

### Profile
最后用传统的神秘仪式来收尾
![profile.jpg](https://img.picgo.net/2024/07/25/profile9c120624616a68f7.jpg)

🏵️🌺🌸七月完结散花🌸🌺🏵️

期待八月，欢迎来自五湖四海的 agent 参加[长沙八月 IFS](https://fevgames.net/ifs/event/?e=27174)，感受长沙文化的独特魅力。 

<center class='half'>
	<img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/enla6029a0f12dacc68.jpg"/>
    <img style="display: inline-block" width='45%' src="https://img.picgo.net/2024/07/25/res774893f336154fa7.png"/>
</center>