+++
title="经长沙人夏日任务大作战"
date=2024-08-03
in_search_index = true

tags = ["ingress", "mission"]

[extra]
toc = true
show_comment = true
cc_license = true
+++

什么！？来长沙不知道做什么任务，看这里就够了！

<!--more-->

## 缘由
---
回想起两年前，我初来乍到，发现长沙有很多好看的任务，美美地做了许多任务，但是也被一些任务坑过，至今还有一个六图任务只做了一个 ~~(是我懒)~~ 。当时做一些大型任务，都会写一篇博客，记录行走的路线，有哪些坑，任务过程中有什么有趣的事情，中午吃啥都事无巨细地写下来。

其中长沙任务问答多也是本地玩家人尽皆知的痛点了，在不知道答案的前提下，是绝不敢贸然前行的，毕竟有的答案是真的连词条都搜索不出来，基本属于是自创答案式个人专属任务，后来者能否完成全靠 agent 口口相传和记忆力的准确性了。还有的任务则需要在墓地上蹿下跳几个来回，清明节来做一定很热闹（bushi

今年大家陆续申请了很多简单并容易快速完成的 6 图或 12 图任务，每每有来长沙玩的 agent 在群里问有什么推荐任务时，也都会优先推荐这些。但难免有些美丽地任务涉及到高校允不允许外人进入、问题答案全不全、是否在装修施工等问题，我们无法给到准确答复；此外现有的 Trello 平台虽具有评论功能但收录任务数太少、且无法继续维护，Bannergress 缺少评论功能无法排雷；以上种种，让我们萌发出做一个推荐自己城市任务的想法。

但具体的实现形式有待商榷，大家也是集思广益，什么做个网页介绍长沙的 PO场/任务/IFS 啦、搞个前端啦（此处@不看前文自动跳出来的`XS`）、写个 notion 啦，最终还是觉得 tg 的频道最实用，只是需要手动管理标签比较麻烦，但频道很适合一些不想加群只想默默做任务的 agent 来查找。

在建设频道的过程中，由于 tg 的中文搜索 bug 不方便查找到具体任务，我们想到用 “#+任务名称“ 和固化标签来实现，`@呼呼呼` 提议写一个置顶把任务排序并 link 到每个任务，直接导航到指定消息来实现快速查找。非常感谢他，这个建议很实用~

![mission chat](https://img.picgo.net/2024/07/25/mission_chata0eccf6dfbab82e1.png)

经过几天的疯狂编辑，我们的任务频道已初具模型，基本涵盖了长沙现存所有任务，并且每个任务下面都可以评论，欢迎大家关注频道鸭🦆。

频道链接：
{{< alert "telegram" >}}
[**长沙任务频道**](https://t.me/csxmission)
{{< /alert >}}

## 推荐任务
---
再来介绍几个我喜欢的任务，排名不分先后

### 任务一

可爱橘猫，简单又可爱，谁不喜欢呢

<center>
<img width='60%' value="action" src = "https://api.bannergress.com/bnrs/pictures/00333032ef0a2e353b549909b8f1285e"/>
</center>

### 任务二

EVA，老二次元的最爱

<center>
<img width='60%' value="action" src = "https://api.bannergress.com/bnrs/pictures/2cf264196ef89a1ae2c97cc12652ef03"/>
</center>

### 任务三

城市地标必做 I Love Changsha，城市夜景也是我最喜欢的一种任务，这个任务强度是有点大，需要充裕的时间

<center>
<img width='50%' value="action" src = "https://api.bannergress.com/bnrs/pictures/4efc203c3f3e0af21d2e9fcbad960feb"/>
</center>

### 任务四

橘洲烟火，记录最美瞬间

<center>
<img width='60%' value="action" src = "https://api.bannergress.com/bnrs/pictures/160f6761ce0059ba79fa41be0626719e"/>
</center>

### 任务五

城市地铁系列任务，三个站点，六行任务，玩转长沙地铁

<center>
<img width='60%' value="action" src = "https://img.picgo.net/2024/08/03/subway34a202c60669b727.png"/>
</center>

## 温馨提示

🌡暑期的长沙火热，刷任务务必做好避暑措施

BTW，[长沙 IFS](https://fevgames.net/ifs/event/?e=27174) 欢迎各位 agent 一起攻略长沙任务

作者：`@monsoonlh`, `@primalwildness`