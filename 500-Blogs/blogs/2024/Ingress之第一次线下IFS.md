+++
title="Ingress之第一次线下IFS"
date=2024-01-06
in_search_index = true

tags = ["ingress", "ifs"]

[extra]
toc = true
show_comment = true
cc_license = true
+++

记录本人第一次参与线下IFS活动（也是疫情后的第一次），地点在长沙国金IFS。

<!--more-->

**参与人员**

| Enlighten | Resistance |
| ---- | ---- |
| @mContext | @primalwildness |
| @Yuwan2333 | @Cryolitia |
| @monsoonlh | @StormAndre |
| @Oliverpi | @xchufeng |
| @svnerd | @XXXXssss |
| @hanaxilo |  |
| @coinicon |  |
| @MrNaux |  |
| @W7lw |  |

## 初来乍到
---
下午四点刚好到达国金广场，前面围着一群人，毫无疑问就是各位 agents 特有的阵形。庞大的阵形把安保也吸引过来了，毕竟要聚众搞事。
等人齐后先来一张合照，为了证明我们是乖孩子，于是将计就计，让安保给我们拍照。
![full](https://cdnjson.com/images/2024/01/07/IMG_20240107_161547.jpg)

## 解密
---
拍完照，大家跟着“摄政王”(@monsoonlh) 做 IFS 任务（小猫任务）。
快到 5：30 左右，走到黄兴广场时，我们发现解密任务还没有做（passcode 任务貌似需要两小时内做完），于是火急火燎地跑回国金。

任务邮件其实是前一天发的，里面包含了需要 hack 的图片，以及 passcode 的形式。
密码形式是：`xxx##keyword###xx`，其中 `#` 填写数字，`x` 填写字母。
解密图片如下：

<center class='half'>
    <img width='70%' src="https://fevgames.net/ifs/onsite/2024-01/137167881012102794911089878888106897346726478.jpg"/>
</center>

解密的流程就是，在你 hack 指定的 po，就会获得一份 Media，将获取所有 Media 的信息填入 passcode 模板中就能获得完整的 passcode。
<center class='half'>
    <img width='20%' src="https://cdnjson.com/images/2024/01/07/Screenshot_2024-01-06-11-05-58-74_4bd081dab89a3c82e75a2ae7696e5187.jpg"/>
    <img width='20%' src="https://cdnjson.com/images/2024/01/07/Screenshot_2024-01-07-00-24-18-60_f8fe15ca462757b2491b7e33b1bea436.jpg"/>
</center>
(左边是路线，右边是 Media )

回到国金，我们兵分两路，一边从起始 po 开始 hack，另一边从末尾 po 开始 hack。
原本还以为在任务期间会有另外一封邮件发给 Leader，但是并没有，最后，我们按照顺序，把 passcode 给弄个出来。

## 开饭
---
忙乎玩解密，就到了最重要的开饭时间辣。在火锅和湘菜的犹豫不决下，我们最终还是选择了湘菜。开饭前，先来面基的传统艺能——大合照。

![ingress_phones](https://cdnjson.com/images/2024/01/07/IMG_20240107_004347_685.jpg)

这次尝试了 `老街鱼嘴巴`。鱼头做的还真行，入味到鱼骨里面了，嗦鱼头很下饭。还有一个玉米酥也挺好吃的，甜口酥脆感。

![dish](https://cdnjson.com/images/2024/01/07/IMG_20240106_191919.jpg)

## 饭后娱乐
---
最后就是大家喜闻乐见的超频活动。顺便再帮摄政王升级到 Lv12 。

## 收获
---

三张 Biocard，以及懒军和绿军快乐相处的一天。（还有没有发的牌子）
<center class='half'>
    <img width='50%' src="https://cdnjson.com/images/2024/01/07/IMG_20240106_233237.jpg"/>
</center>